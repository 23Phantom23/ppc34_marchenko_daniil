package ex02;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import ex01.Item2d;

/**
 * ConcreteProduct (������ �������������� Factory Method)<br>
 * ���������� �������, ���������� � ����������� �����������
 * 
 * @author Marchenko Daniil
 * @version 1.0
 * @see View
 */
public class ViewResult implements View {
	/** ��� �����, ������������ ��� ������������ */
	private static final String FNAME = "items.bin";
	/** ���������� ���������� �������� ��� ���������� �� ��������� */
	private static final int DEFAULT_NUM = 10;
	/** ��������� ���������� � ����������� ���������� */
	private ArrayList<Item2d> items = new ArrayList<Item2d>();

	/**
	 * �������� {@linkplain ViewResult#ViewResult(int n) ViewResult(int n)} �
	 * ���������� {@linkplain ViewResult#DEFAULT_NUM DEFAULT_NUM}
	 */
	public ViewResult() {
		this(DEFAULT_NUM);
	}

	/**
	 * �������������� ��������� {@linkplain ViewResult#items}
	 * 
	 * @param n ��������� ���������� ���������
	 */
	public ViewResult(int n) {
		for (int ctr = 0; ctr < n; ctr++) {
			items.add(new Item2d());
		}
	}

	/**
	 * �������� �������� {@linkplain ViewResult#items}
	 * 
	 * @return ������� �������� ������ �� ������ {@linkplain ArrayList}
	 */
	public ArrayList<Item2d> getItems() {
		return items;
	}

	/**
	 * ��������� �������� �������
	 * 
	 * @param Sum �������� ����������� �������
	 * @return ��������� ���������� �������
	 */
	private int calc(double Sum) {
		int Sums = (int)Sum;
		if(Sums < 0) {
			Sums = Sums * -1;
		}
		String SumStr = Integer.toBinaryString(Sums);
		
		int length = SumStr.length();
	    int MaxCountOne = 0;
	    int CountOne = 0;
	    for(int j = length; j!=0; j--){
	        char c = SumStr.charAt(length-j);
	        if (c == '1'){
	            CountOne++;
	        } else {
	            if(MaxCountOne > CountOne){
	                CountOne = 0;
	            }else{
	                MaxCountOne = CountOne;
	                CountOne = 0;
	            }
	        }
	    }
	    if(MaxCountOne > CountOne){
	                CountOne = 0;
	            }else{
	                MaxCountOne = CountOne;
	                CountOne = 0;
	            }
		return MaxCountOne;
	}

	/**
	 * ��������� �������� ������� � ��������� ��������� � ���������
	 * {@linkplain ViewResult#items}
	 * 
	 * @param stepX ��� ���������� ���������
	 */
	public void init(double stepX) {
		double x = 0.0;
		for (Item2d item : items) {
			item.setSumCountInt(x, calc(x));
			x += stepX;
		}
	}

	/**
	 * �������� <b>init(double stepX)</b> �� ��������� ��������� ���������<br>
	 * {@inheritDoc}
	 */
	@Override
	public void viewInit() {
		init(Math.random() * 360.0);
	}

	/**
	 * ���������� ������ {@linkplain View#viewSave()}<br>
	 * {@inheritDoc}
	 */
	@Override
	public void viewSave() throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
		os.writeObject(items);
		os.flush();
		os.close();
	}

	/**
	 * ���������� ������ {@linkplain View#viewRestore()}<br>
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void viewRestore() throws Exception {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
		items = (ArrayList<Item2d>) is.readObject();
		is.close();
	}

	/**
	 * ���������� ������ {@linkplain View#viewHeader()}<br>
	 * {@inheritDoc}
	 */
	@Override
	public void viewHeader() {
		System.out.println("Results:");
	}

	/**
	 * ���������� ������ {@linkplain View#viewBody()}<br>
	 * {@inheritDoc}
	 */
	@Override
	public void viewBody() {
		for (Item2d item : items) {
			System.out.printf("(%.0f; %.3f) ", item.getSum(), item.getCountInt());
		}
		System.out.println();
	}

	/**
	 * ���������� ������ {@linkplain View#viewFooter()}<br>
	 * {@inheritDoc}
	 */
	@Override
	public void viewFooter() {
		System.out.println("End.");
	}

	/**
	 * ���������� ������ {@linkplain View#viewShow()}<br>
	 * {@inheritDoc}
	 */
	@Override
	public void viewShow() {
		viewHeader();
		viewBody();
		viewFooter();
	}
}