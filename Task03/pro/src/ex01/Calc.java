package ex01;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * �������� ���������� ������� ��� ���������� � ����������� �����������.
 * 
 * @author Marchenko Daniil
 * @version 1.0
 */

public class Calc {
	/** ��� �����, ������������ ��� ������������. */
	private static final String FNAME = "Item2d.bin";
	/** ��������� ��������� ����������. ������ ������ {@linkplain Item2d} */
	private Item2d result;

	/** �������������� {@linkplain Calc#result} */
	public Calc() {
		result = new Item2d();
	}

	/**
	 * ���������� �������� {@linkplain Calc#result}
	 * 
	 * @param result - ����� �������� ������ �� ������ {@linkplain Item2d}
	 */
	public void setResult(Item2d result) {
		this.result = result;
	}

	/**
	 * �������� �������� {@linkplain Calc#result}
	 * 
	 * @return ������� �������� ������ �� ������ {@linkplain Item2d}
	 */
	public Item2d getResult() {
		return result;
	}

	/**
	 * ��������� �������� �������.
	 * 
	 * @param Sum - �������� ����������� �������.
	 * @return ��������� ���������� �������.
	 */
	private int calc(double Sum) {
		int Sums = (int)Sum;
		if(Sums < 0) {
			Sums = Sums * -1;
		}
		String SumStr = Integer.toBinaryString(Sums);
		
		int length = SumStr.length();
	    int MaxCountOne = 0;
	    int CountOne = 0;
	    for(int j = length; j!=0; j--){
	        char c = SumStr.charAt(length-j);
	        if (c == '1'){
	            CountOne++;
	        } else {
	            if(MaxCountOne > CountOne){
	                CountOne = 0;
	            }else{
	                MaxCountOne = CountOne;
	                CountOne = 0;
	            }
	        }
	    }
	    if(MaxCountOne > CountOne){
	                CountOne = 0;
	            }else{
	                MaxCountOne = CountOne;
	                CountOne = 0;
	            }
		return MaxCountOne;
	}

	/**
	 * ��������� �������� ������� � ��������� ��������� � �������
	 * {@linkplain Calc#result}
	 * 
	 * @return ��������� ���������� ������� calc
	 * @param Sum - �������� ����������� �������.
	 */
	public int init(double Sum) {
		double Sums = (Math.pow(10*Math.cos(Sum), 2)+Math.pow(10*Math.cos(Sum), 3));
		result.setSum(Sums);
		return result.setCountInt(calc(Sums));
	}

	/** ������� ��������� ����������. */
	public void show() {
		System.out.println(result);
	}

	/**
	 * ��������� {@linkplain Calc#result} � ����� {@linkplain Calc#FNAME}
	 * 
	 * @throws IOException
	 */
	public void save() throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
		os.writeObject(result);
		os.flush();
		os.close();
	}

	/**
	 * ��������������� {@linkplain Calc#result} �� ����� {@linkplain Calc#FNAME}
	 * 
	 * @throws Exception
	 */
	public void restore() throws Exception {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
		result = (Item2d) is.readObject();
		is.close();
	}
}