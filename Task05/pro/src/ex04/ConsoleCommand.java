package ex04;

/**
 * ��������� ���������� �������; ������ Command
 * 
 * @author Marchenko Daniil
 * @version 1.0
 */
public interface ConsoleCommand extends Command {
	/**
	 * ������� ������� �������; ������ Command
	 * 
	 * @return ������ ������� �������
	 */
	public char getKey();
}