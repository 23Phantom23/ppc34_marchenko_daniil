package ex04;

/**
 * ���������� � ����������� �����������; c������� ���������� ������������ ������
 * main()
 * 
 * @author Marchenko Daniil
 * @version 4.0
 * @see Main#main
 */
public class Main {
	/**
	 * ����������� ��� ������� ���������; �������� �����
	 * {@linkplain Application#run()}
	 * 
	 * @param args ��������� ������� ���������
	 */
	public static void main(String[] args) {
		Application app = Application.getInstance();
		app.run();
	}
}