package ex02;

/**
 * Creator (������ �������������� Factory Method)<br>
 * ��������� �����, "�����������" �������
 * 
 * @author Marchenko Daniil
 * @version 1.0
 * @see Viewable#getView()
 */
public interface Viewable {
	/** ������ ������, ����������� {@linkplain View} */
	public View getView();
}