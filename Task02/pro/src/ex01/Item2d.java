package ex01;

import java.io.Serializable;

/**
 * ������ �������� ������ � ��������� ����������.
 * 
 * @author Marchenko Daniil
 * @version 1.0
 */
public class Item2d implements Serializable {
	/** �������� ����������� �������. */
// transient
	private double Sum;
	/** ��������� ���������� �������. */
	private int CountInt;
	/** ������������� ��������������� ��������� */
	private static final long serialVersionUID = 1L;

	/** �������������� ���� {@linkplain Item2d#Sum}, {@linkplain Item2d#CountInt} */
	public Item2d() {
		Sum = .0;
		CountInt = 0;
	}

	/**
	 * ������������� �������� �����: ��������� � ���������� ���������� �������.
	 * 
	 * @param Sum - �������� ��� ������������� ���� {@linkplain Item2d#Sum}
	 * @param CountInt - �������� ��� ������������� ���� {@linkplain Item2d#CountInt}
	 */
	public Item2d(double Sum, int CountInt) {
		this.Sum = Sum;
		this.CountInt = CountInt;
	}

	/**
	 * ��������� �������� ���� {@linkplain Item2d#Sum}
	 * 
	 * @param Sum - �������� ��� {@linkplain Item2d#Sum}
	 * @return �������� {@linkplain Item2d#Sum}
	 */
	public double setSum(double Sum) {
		return this.Sum = Sum;
	}

	/**
	 * ��������� �������� ���� {@linkplain Item2d#Sum}
	 * 
	 * @return �������� {@linkplain Item2d#Sum}
	 */
	public double getSum() {
		return Sum;
	}

	/**
	 * ��������� �������� ���� {@linkplain Item2d#CountInt}
	 * 
	 * @param CountInt - �������� ��� {@linkplain Item2d#CountInt}
	 * @return �������� {@linkplain Item2d#CountInt}
	 */
	public int setCountInt(int CountInt) {
		return this.CountInt = CountInt;
	}

	/**
	 * ��������� �������� ���� {@linkplain Item2d#CountInt}
	 * 
	 * @return �������� {@linkplain Item2d#CountInt}
	 */
	public double getCountInt() {
		return CountInt;
	}

	/**
	 * ��������� �������� {@linkplain Item2d#Sum} � {@linkplain Item2d#CountInt}
	 * 
	 * @param Sum - �������� ��� {@linkplain Item2d#Sum}
	 * @param CountInt - �������� ��� {@linkplain Item2d#CountInt}
	 * @return this
	 */
	public Item2d setSumCountInt(double Sum, int CountInt) {
		this.Sum = Sum;
		this.CountInt = CountInt;
		return this;
	}

	/**
	 * ������������ ��������� ���������� � ���� ������.<br>
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "Sum = " + Sum + ", CountInt = " + CountInt;
	}

	/**
	 * ������������� ��������������� �����.<br>
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item2d other = (Item2d) obj;
		if (Double.doubleToLongBits(Sum) != Double.doubleToLongBits(other.Sum))
			return false;
// �������� ��������� ���������� ���������� �������
		if (Math.abs(Math.abs(CountInt) - Math.abs(other.CountInt)) > .1e-10)
			return false;
		return true;
	}
}